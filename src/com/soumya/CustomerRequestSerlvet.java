package com.soumya;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.soumya.PremiumCalc;

/**
 * Servlet implementation class InsuranceRequest
 */
@WebServlet("/CustomerRequestSerlvet")
public class CustomerRequestSerlvet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustomerRequestSerlvet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String name = request.getParameter("name");
	    String ageString = request.getParameter("age");
	    String gender = request.getParameter("gender");
	    boolean hypertension = request.getParameter("hypertension") != null;
	    boolean bPressure = request.getParameter("bloodPressure") != null;
	    boolean bSugar = request.getParameter("bloodSugar") != null;
	    boolean overweight = request.getParameter("overweight") != null;
	    boolean smoking = request.getParameter("smoking") != null;
	    boolean alcohol = request.getParameter("alcohol") != null;
	    boolean exercise = request.getParameter("exercise") != null;
	    boolean drugs = request.getParameter("drugs") != null;
	    
	    int age=Integer.parseInt(ageString);
	    
	    double premium= PremiumCalc.calculatePremium(gender, age, hypertension, bPressure, bSugar, overweight, smoking, alcohol, exercise, drugs);
	    
	    request.setAttribute("name", name);
	    request.setAttribute("premium", premium);
	    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/PremiumDetails.jsp");
	    dispatcher.forward(request,response);
	
	}

}